/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;


/**
 *
 * @author vagner
 */
public class ContadorPalavras {
    
    private BufferedReader reader;
    
    private Map<String, Integer> palavra_conta = new TreeMap<>();
    private Map<String, Integer> palavra_contav = new TreeMap<>();
    private String linha,palavra;
    private int indice_map;
    
    public ContadorPalavras(String caminho) throws FileNotFoundException, IOException{
        this.reader=new BufferedReader(new FileReader(caminho));
    }
    
    public Map getPalavras() throws IOException{
    
        while ((linha = reader.readLine()) != null) {
            //System.out.println(linha.length());
            for(int j=0;j<linha.length();j++){  // linha não pega \n ou \r
                palavra="";
                while(j!=linha.length()&&       // caracteres aserem excluídos
                        linha.charAt(j)!=' '&&
                        linha.charAt(j)!='.'&&linha.charAt(j)!='!'&&
                        linha.charAt(j)!='?'&&linha.charAt(j)!='"'&&
                        linha.charAt(j)!=','&&linha.charAt(j)!=':'&&
                        linha.charAt(j)!='('&&linha.charAt(j)!=')')
                {
                    palavra=palavra+linha.charAt(j);    // forma palavra
                    j++;
                }
                if(palavra_conta.containsKey(palavra))  // Busca palavra - Aumenta contagem
                    palavra_conta.put(palavra,palavra_conta.get(palavra)+1);
                else if(!palavra.equals(""))
                    palavra_conta.put(palavra,1);
            }
        }
        reader.close();
        //Collections.sort();
/*        for (Map.Entry<String, Integer> entry : palavra_conta.entrySet()) {
            String str=(entry.getKey()+","+entry.getValue());
            writer.write(str);
            writer.newLine();
        }*/
/*        Map<String, Integer> palavrac = new TreeMap<>(palavra_conta);
        List<Entry<String, Integer>> palavrai = 
                new ArrayList<Entry<String, Integer>>(palavrac.entrySet());
        Collections.sort(palavrai,new Comparator<Entry<String,Integer>>(){
            @Override
                public int compare(Entry<String,Integer> e1, Entry<String,Integer> e2) {
                    return e2.getValue().compareTo(e1.getValue());
                }
        });
        
        indice_map = palavrai.size();
        for(int i=0;i < indice_map; i++){
            String key = palavrai.get(i).getKey();
            Integer value = palavrai.get(i).getValue();
            palavra_contav.entrySet();
        //    System.out.println(key + " => " + value);
        } */
       
        //Set s = palavrac.entrySet();
        
        //Iterator it = s.iterator(); //Organiza campos pela chave
        
        //while ( it.hasNext() ) {
        //    Map.Entry entry = (Map.Entry) it.next();
            // entry fornece valor de String=Integer
        //    String key = (String) entry.getKey();
        //    int value = (int) entry.getValue();
            //palavrai.put(value,key);
        //    System.out.println(key + " => " + value);
        //}//while

        

        
/*        Set t = palavrai.entrySet();
        System.out.println(t);
        Iterator ti = t.iterator();
        while (ti.hasNext()){
            Map.Entry entryi = (Map.Entry) ti.next();
            String keyi = (String) entryi.getValue();
            int valuei = (int) entryi.getKey();
            System.out.println(keyi + " => " + valuei);
        }*/

        return palavra_conta;
    }
    
}