
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import utfpr.ct.dainf.if62c.pratica.ContadorPalavras;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica72 {
    
    private static Map<String, Integer> palavra_lista = new TreeMap<String, Integer>();
    private static ContadorPalavras palavra_conta;
    private static String caminho;
    private static int indice_map;
    private static BufferedWriter writer;
    

    public static void main(String[] args) throws FileNotFoundException, IOException {
        //System.out.println("Olá, Java!");
        
        System.out.println("Entre com o caminho completo da localização do arquivo texto : ");
        Scanner scan = new Scanner(System.in);
        caminho=scan.nextLine();
        try{
        palavra_conta = new ContadorPalavras(caminho);
        palavra_lista = palavra_conta.getPalavras();
        Map<String, Integer> palavrac = new TreeMap<>(palavra_lista);
        List<Map.Entry<String, Integer>> palavrai = 
                new ArrayList<Map.Entry<String, Integer>>(palavrac.entrySet());
        Collections.sort(palavrai,new Comparator<Map.Entry<String,Integer>>(){
            @Override
                public int compare(Map.Entry<String,Integer> e1, Map.Entry<String,Integer> e2) {
                    return e2.getValue().compareTo(e1.getValue());
                }
        });
        writer = new BufferedWriter(new FileWriter(caminho+".out"));
        indice_map = palavrai.size();
        for(int i=0;i < indice_map; i++){
            String key = palavrai.get(i).getKey();
            Integer value = palavrai.get(i).getValue();
            palavra_lista.entrySet();
            String stro=(key+","+value);
            writer.write(stro);
            writer.newLine();
            System.out.println(key + " => " + value);
        }
        writer.close();
        } catch(FileNotFoundException ex) { System.out.println("Arquivo não encontrado: "+ex.getLocalizedMessage()); }
        catch(IOException iex) { System.out.println("Erro na leitura do arquivo: "+iex.getLocalizedMessage()); }
        
        
        //indice_map = palavra_lista.size();
        //for (Map.Entry<String, Integer> entry : palavra_lista.entrySet()) {
        //    String str=(entry.getKey()+","+entry.getValue());
        //}
        
        
        
        
        
        
        //for(int i=0;i < indice_map; i++){
        //    String key = palavra_lista.keySet().;
        //    Integer value = palavra_lista.get(i).getValue();
        //    System.out.println(key + " => " + value);
        //}
        //for (Map.Entry<String, Integer> entry : palavra_lista.entrySet()) {
        //    System.out.println(entry.getKey()+" : "+entry.getValue());
        //}
        
    }
}
